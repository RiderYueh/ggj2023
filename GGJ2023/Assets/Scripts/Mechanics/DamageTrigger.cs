using Platformer.Mechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    public float Damage;
    // Start is called before the first frame update

    // Update is called once per frame

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyBe>().OnAttacked(Mathf.RoundToInt(Damage));
            
        }
    }

}
