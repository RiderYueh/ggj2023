using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfRotating : MonoBehaviour
{   
    public float rotatingSpeed;


    // Update is called once per frame
    void FixedUpdate()
    {
        if(rotatingSpeed >0) {
            this.transform.eulerAngles += new Vector3(0, 0, rotatingSpeed * Time.deltaTime);
        }
    }
}
