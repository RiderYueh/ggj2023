using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillController : MonoBehaviour
{   
    public bool[]   learntSkill;
    public float[]  skillDmg;
    public float[]  skillInterval;
    public float[]  skillSize;
    public float[]  skillDuration;
    public GameObject[] skillPrefabs;
    private GameObject Player;


    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        Player = GameObject.Find("Player");
        Invoke("activateLearntSkills", 2.5f);
    }
    void UpdateStates()
    {
        learntSkill     = MainData.Instance.learntSkill;
        skillDmg        = MainData.Instance.skillDmg;
        skillInterval   = MainData.Instance.skillInterval;
        skillSize       = MainData.Instance.skillSize;
        skillDuration   = MainData.Instance.skillDuration;
    }


    public void cancelAllSkills(){
        CancelInvoke("skill_0_AreaAttack");
        CancelInvoke("skill_1_ThrowAttack");
        CancelInvoke("skill_2_ShockwaveAttack");
        CancelInvoke("Skill_3_OrbitalAttack");
    }

    void activateLearntSkills(){
        if (learntSkill[0]) InvokeRepeating("skill_0_AreaAttack"        , 0f, skillInterval[0]);
        if (learntSkill[1]) InvokeRepeating("skill_1_ThrowAttack"       , 0f, skillInterval[1]);
        if (learntSkill[2]) InvokeRepeating("skill_2_ShockwaveAttack"   , 0f, skillInterval[2]);
        if (learntSkill[3]) InvokeRepeating("Skill_3_OrbitalAttack"     , 0f, skillInterval[3]);
    }

    void skill_0_AreaAttack(){
        //Debug.Log("skill_0_AreaAttack");

        GameObject ObjToDestroy = Instantiate(skillPrefabs[0], this.transform);
        ObjToDestroy.transform.position = Player.transform.position;
        //Debug.Log("發出技能0重擊地板的聲音");
        AudioManager.instance.PlaySound(AudioManager.ESound.skill00);
        ObjToDestroy.transform.localScale *=  skillSize[0];
        foreach (DamageTrigger aa in ObjToDestroy.GetComponentsInChildren<DamageTrigger>())
        {
            aa.Damage = skillDmg[0];
        }
        Destroy(ObjToDestroy, skillDuration[0]);
    }

    void skill_1_ThrowAttack(){
        //Debug.Log("skill_1_ThrowAttack");

        float throwForce = 150f;
        float flipDirection = 1;
        if(Player.transform.localScale.x < 0)
        {
            flipDirection = -1;
        }

        GameObject ObjToDestroy = Instantiate(skillPrefabs[1], Player.transform.parent);
        ObjToDestroy.transform.position = Player.transform.position + new Vector3(0,1,0);
        //Debug.Log("發出技能1投擲的聲音");
        AudioManager.instance.PlaySound(AudioManager.ESound.skill01);
        //ObjToDestroy.transform.localScale *= skillSize[1];
        ObjToDestroy.GetComponent<DamageTrigger>().Damage = skillDmg[1];
        ObjToDestroy.GetComponent<Rigidbody2D>().AddForce(new Vector2(1* flipDirection, 1) * throwForce);
        ObjToDestroy.GetComponent<Rigidbody2D>().AddTorque(-50 * flipDirection);
        //45度角丟出
        Destroy(ObjToDestroy, skillDuration[1]);
    }

    void skill_2_ShockwaveAttack(){
        //Debug.Log("skill_2_ShockwaveAttack");
        AudioManager.instance.PlaySound(AudioManager.ESound.skill03);
        float xOffset = 2f;
        float flipDirection = 1;
        if (Player.transform.localScale.x < 0)
        {
            flipDirection = -1;
        }
        GameObject ObjToDestroy = Instantiate(skillPrefabs[2], this.transform);
        ObjToDestroy.transform.position = Player.transform.position;
        //Debug.Log("發出技能2衝擊的聲音");

        ObjToDestroy.transform.localScale = new Vector3(skillSize[2] * flipDirection, skillSize[2], skillSize[2]);

        //ObjToDestroy.transform.position += new Vector3(xOffset, 0, 0);

        foreach (DamageTrigger aa in ObjToDestroy.GetComponentsInChildren<DamageTrigger>())
        {
            aa.Damage = skillDmg[2];
        }
        //ObjToDestroy.GetComponent<Rigidbody2D>().AddForce(new Vector2(2, 0) * flyspeed);
        //45度角丟出
        Destroy(ObjToDestroy, skillDuration[1]);
    }

    void Skill_3_OrbitalAttack(){
        //Debug.Log("Skill_3_OrbitalAttack");
        AudioManager.instance.PlaySound(AudioManager.ESound.skill03);
        GameObject ObjToDestroy = Instantiate(skillPrefabs[3], Player.transform.parent);
        ObjToDestroy.transform.position = Player.transform.position;
        //Debug.Log("發出技能3投射物呼嘯旋轉的聲音");
        ObjToDestroy.transform.localScale *= skillSize[3];

        foreach (DamageTrigger aa in ObjToDestroy.GetComponentsInChildren<DamageTrigger>())
        {
            aa.Damage = skillDmg[3];
        }
        Destroy(ObjToDestroy, skillDuration[3]);
    }


}
