using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFollowEnemy : MonoBehaviour
{
    public Transform target;
    public EnemyBe enemy;
    [Range(0,1)]
    public float followSpeed = 0.8f;
    public Slider slider;
    public TMPro.TextMeshProUGUI text;


    public void Init(Transform target, EnemyBe enemy)
    {
        this.target = target;
        this.enemy = enemy;
        text.text = enemy.nameStr;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = (float)enemy.HP / enemy.MAX_HP;

        if (target != null)
        {
            Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, target.position);
            transform.position = Vector2.Lerp(transform.position, screenPoint, followSpeed);
        }

        if (target == null
            || enemy == null
            || enemy.HP <= 0)
        {
            Destroy(gameObject);
        }
    }
}
