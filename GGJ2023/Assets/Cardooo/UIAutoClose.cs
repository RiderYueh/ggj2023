using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAutoClose : MonoBehaviour
{
    public float openTime = 2;
    public float curTime;
    private void OnEnable()
    {
        curTime = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        curTime += Time.deltaTime;
        if (curTime < openTime)
            return;

        curTime = 0;
        gameObject.SetActive(false);
    }
}
