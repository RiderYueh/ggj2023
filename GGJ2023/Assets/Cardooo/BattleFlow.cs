using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FSM
{
    public State CurState { get; private set; } = null;

    public void GotoState(State state)
    {
        if (CurState != null)
            CurState.leave();

        CurState = state;
        CurState.start();
    }

    public void update()
    {
        if (CurState != null)
            CurState.update();
    }
}

public class State
{
    protected FSM fsm;

    public State(FSM fsm)
    {
        this.fsm = fsm;
    }

    public virtual void start()
    {

    }

    public virtual void update()
    {

    }

    public virtual void leave()
    {

    }
}

public class BattleFlow : MonoBehaviour
{
    public static BattleFlow Inst;
    public GameObject winPanel;
    public GameObject lossPanel;
    public GameObject enemyInfo;
    public Transform enemyInfoRoot;

    FSM fsm = new FSM();

    // Start is called before the first frame update
    private void Awake()
    {
        Inst = this;
    }
    void Start()
    {
        
        fsm.GotoState(new BattleState(fsm));
    }

    // Update is called once per frame
    void Update()
    {
        fsm.update();
    }

    private void OnDestroy()
    {
        Inst = null;
    }

    public void RestartBattle()
    {
        fsm.GotoState(new RestartState(fsm));
    }

    [ContextMenu("WinBattle")]
    public void WinBattle()
    {
        fsm.GotoState(new WinState(fsm));
    }

    public void showLossPanel(bool enable)
    {
        lossPanel.SetActive(enable);
    }

    public void BackHome()
    {
        if(MainData.Instance.StateNow >=4)
        {
            Debug.Log("BE");
            SceneManager.LoadScene(7);
        }
        else
        {
            Debug.Log("Go home");
            SceneManager.LoadScene(6);
        }
        
    }

    public void CreateEnemyInfo(EnemyBe enemy)
    {
        var info =Instantiate(enemyInfo, enemyInfoRoot).GetComponent<UIFollowEnemy>();
        info.Init(enemy.transform, enemy);
    }

    class BattleState : State
    {
        public BattleState(FSM fsm) : base(fsm){ }

        public override void start()
        {
            base.start();
            MainData.Instance.winBattle = false;
        }

        public override void update()
        {
            base.update();

            if (MainData.Instance.HP <= 0)
            {
                fsm.GotoState(new LossState(fsm));
                return;
            }

            if(Input.GetKeyDown("m"))
            {
                MainData.Instance.winBattle = true;
                if (MainData.Instance.winBattle)
                {
                    fsm.GotoState(new WinState(fsm));
                    return;
                }
            }
            if (MainData.Instance.winBattle)
            {
                fsm.GotoState(new WinState(fsm));
                return;
            }
        }
    }

    class WinState : State
    {
        public WinState(FSM fsm) : base(fsm) { }

        public override void start()
        {
            base.start();
            BattleFlow.Inst.winPanel.SetActive(true);
            AudioManager.instance.PlaySound(AudioManager.ESound.Win);
        }
    }

    class LossState : State
    {
        public LossState(FSM fsm) : base(fsm) { }

        public override void start()
        {
            base.start();
            BattleFlow.Inst.showLossPanel(true);
        }
    }

    class RestartState : State
    {
        public RestartState(FSM fsm) : base(fsm) { }

        public override void start()
        {
            base.start();
            BattleFlow.Inst.showLossPanel(false);
            EnemyPool.Inst.Reset();
            MainData.Instance.StageRePlay();
            MainPlayer.instance.UpdateHP();
            fsm.GotoState(new BattleState(fsm));
        }
    }
}
