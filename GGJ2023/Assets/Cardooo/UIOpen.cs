using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIOpen : MonoBehaviour
{
    public float cd = 1f;
    public float fadeoutCd = 1f;
    public float fadeoutSpeed = 1f;

    void Start()
    {
        Invoke("gotoScene", cd);        
    }

    public void gotoScene()
    {
        SceneManager.LoadScene(1);
    }
}
