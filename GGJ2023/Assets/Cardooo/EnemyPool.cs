using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPool : MonoBehaviour
{
    public Slider slider;
    public TMPro.TextMeshProUGUI notice;

    public int curRound;
    public int[] createList = { 0, 5, 10 };

    public static EnemyPool Inst;

    [Range(-40,-11)]
    public float createX_Min;
    [Range(-40, -11)]
    public float createX_Max;
    [Range(-4, 2)]
    public float createY_Min;
    [Range(-4, -2)]
    public float createY_Max;    
    public List<GameObject> list = new List<GameObject>();
    public List<int> list_2 = new List<int>();

    public List<GameObject> instlist = new List<GameObject>();

    float curCd = 0;

    // Start is called before the first frame update
    void Start()
    {
        Inst = this;
        Reset();
    }

    void clear()
    {

    }

    // Update is called once per frame
    void Update()
    {
        slider.value = curCd / createList[createList.Length - 1];

        // 全部怪物已出生
        if (curRound == createList.Length)
            return;

        curCd += Time.deltaTime;
        if (curCd < createList[curRound])
        {
            return;
        }


        newEnemy(curRound);
        curRound++;
    }

    void newEnemy(int round)
    {
        

        for (int i = 0; i < list_2[round]; i++)
        {
            float x = Random.Range(createX_Min, createX_Max);
            float y = Random.Range(createY_Min, createY_Max);
            
            if(MainPlayer.instance.transform.position.x < -35)
            {
                x = Random.Range(createX_Min + 10 , createX_Max);
            }
            else if (MainPlayer.instance.transform.position.x > -15)
            {
                x = Random.Range(createX_Min, createX_Max - 10);
            }
            else if(Random.Range(0,2) == 0)
            {
                x = Random.Range(MainPlayer.instance.transform.position.x + 5, createX_Max );
            }
            else
            {
                x = Random.Range(createX_Min, MainPlayer.instance.transform.position.x - 5);
            }





            var go = Instantiate(list[round], new Vector3(x, y, 1), Quaternion.identity);
            instlist.Add(go);
            var enemy = go.GetComponent<EnemyBe>();
            if (!enemy.IsBoss)
                notice.text = $"\"{enemy.nameStr} \"\n came to this world！";
            else
                notice.text = $"Boss \"{enemy.nameStr}\"\n Your are fxxked！";
            notice.gameObject.SetActive(true);
        }
        

        

        
    }

    /// <summary>
    /// 刪除＋移除參照
    /// </summary>
    /// <param name="go"></param>
    public void clear(GameObject go)
    {
        instlist.Remove(go);
        Destroy(go);
    }

    /// <summary>
    /// 全部清除
    /// </summary>
    public void Reset()
    {
        foreach (var go in instlist)
        {
            Destroy(go);
        }

        instlist.Clear();

        curRound = 0;
        curCd = 0;
    }
}
