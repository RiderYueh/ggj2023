using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAudio : MonoBehaviour
{
    public GameObject audioManager;
    void Awake()
    {
        if (AudioManager.instance == null)
        {
            Instantiate(audioManager, transform.parent);
            AudioManager.instance.PlaySound(AudioManager.ESound.BGM01);
        }
    }

}
