using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyBe : MonoBehaviour
{
    public string nameStr = "變態的死萊姆";
    public int HP = 10;
    public int MAX_HP = 10;
    public int ATK = 1;
    [Range(0,3)]
    public float MoveSpeed = 1;
    [Header("[擊敗ＢＯＳＳ即戰鬥勝利]")]
    public bool IsBoss = false;

    public bool canDash = false;
    public float cdDash = 2;
    public float curDash = 0;

    public Animator m_Animator;
    private void Start()
    {
       BattleFlow.Inst.CreateEnemyInfo(this);
    }

    private void Update()
    {
        if(!isBeAttacked) FindPlayer();
    }

    void FindPlayer()
    {
        if (!canDash)
        {
            Vector3 playerPos = Vector3.ClampMagnitude(new Vector3(transform.position.x, transform.position.y, transform.position.z) - new Vector3(MainPlayer.instance.transform.position.x, transform.position.y, transform.position.z), 1);
            transform.position -= Time.deltaTime * playerPos * MoveSpeed;
        }
        else
        {
            curDash += Time.deltaTime;
            if (curDash < cdDash)
            {
                return;
            }

            curDash -= cdDash;

            Vector3 playerPos = Vector3.ClampMagnitude(new Vector3(transform.position.x, transform.position.y, transform.position.z) - new Vector3(MainPlayer.instance.transform.position.x, transform.position.y, transform.position.z), 1);
            transform.position -= playerPos * MoveSpeed;
        }

    }

    bool isBeAttacked = false;
    public void OnAttacked(int value = 1)
    {
        if (isBeAttacked) return;
        isBeAttacked = true;
        HP -= value + System.Convert.ToInt32(MainData.Instance.ATK / 2);
        if (HP > 0)
        {
            m_Animator.SetTrigger("hurt");
            //MainPlayer.instance.SlowMotion(0.5f,0.01f);
            AudioManager.instance.PlaySound(AudioManager.ESound.Attack , 0.15f);
        }
        else
        {
            m_Animator.SetTrigger("death");
        }
        StartCoroutine(WaitTime(0.5f , ()=> {
            OnBeAttackEnd();
            if (HP <= 0)
            {                
                AudioManager.instance?.PlaySound(AudioManager.ESound.EnemyDie);
                if (IsBoss)
                {
                    Debug.Log("battle win!");
                    
                    MainData.Instance.winBattle = true;
                }

                EnemyPool.Inst.clear(gameObject);
            }
        }));
    }

    public void OnBeAttackEnd()
    {
        isBeAttacked = false;
    }

    IEnumerator WaitTime(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            MainPlayer.instance.OnHurted(ATK);
        }
    }

}

