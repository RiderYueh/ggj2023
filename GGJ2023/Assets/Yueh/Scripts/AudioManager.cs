using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public List<AudioStruct> list_AudioStruct;
    public AudioSource mainAudioSource;

    public enum ESound
    {
        None,
        BGM01,
        BGM02,
        BGM03,
        BGM04,
        Click,
        Pick,
        Attack,
        Hurt,
        Win,
        EnemyDie,
        skill00,
        skill01,
        skill02,
        skill03,
    }

    [Serializable]
    public struct AudioStruct
    {
        public ESound eSound;
        public AudioClip clip;
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void PlaySound(ESound eSound , float volume = 0.5f)
    {
        foreach (AudioStruct tempAudioStruct in list_AudioStruct)
        {
            if (tempAudioStruct.eSound == ESound.BGM01 || tempAudioStruct.eSound == ESound.BGM02 || tempAudioStruct.eSound == ESound.BGM03 || tempAudioStruct.eSound == ESound.BGM04)
            {
                if (tempAudioStruct.eSound == eSound)
                {
                    mainAudioSource.clip = tempAudioStruct.clip;
                    mainAudioSource.loop = true;
                    mainAudioSource.Play();
                    break;
                }
                
            }
            else if (tempAudioStruct.eSound == eSound)
            {
                mainAudioSource.PlayOneShot(tempAudioStruct.clip, volume);
                break;
            }
        }
    }
}
