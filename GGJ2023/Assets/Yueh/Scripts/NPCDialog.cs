using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NPCDialog : MonoBehaviour
{
    public Text text_point;

    public Text text_HP;
    public Text text_ATK;

    public GameObject go_EnhancePage;

    private int tempHP = 0;
    private int tempATK = 0;
    private int tempPoint = 0;

    bool isOpenEnhance = false;
    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (isOpenEnhance) return;

            isOpenEnhance = true;
            OnGoNext();
        }
    }

    private void OnEnable()
    {
        go_EnhancePage.SetActive(false);
        isOpenEnhance = true;
        StartCoroutine(WaitTime(0.5f, () => {
            isOpenEnhance = false;
        }));
        
        tempATK = MainData.Instance.ATK;
        tempHP = MainData.Instance.MaxHP;
        tempPoint = MainData.Instance.Point;

        Refresh();
    }

    void Refresh()
    {
        text_point.text = tempPoint.ToString();

        text_HP.text = tempHP.ToString();
        text_ATK.text = tempATK.ToString();
    }

    //====btn
    public void OnBtnOK()
    {
        AudioManager.instance.PlaySound(AudioManager.ESound.Click);
        MainData.Instance.ATK = tempATK;
        MainData.Instance.MaxHP = tempHP;
        MainData.Instance.HP = MainData.Instance.MaxHP;
        MainData.Instance.Point = tempPoint;

        MainPlayer.instance.CloseShop();
    }

    public void OnAddHP(bool isAdd)
    {
        if(isAdd)
        {
            if(tempPoint > 0)
            {
                tempPoint -= 1;
                tempHP += 1;
            }
        }
        else
        {
            if (tempHP > MainData.Instance.MinHP)
            {
                tempPoint += 1;
                tempHP -= 1;
            }
        }

        Refresh();
    }

    public void OnAddATK(bool isAdd)
    {
        if (isAdd)
        {
            if (tempPoint > 0)
            {
                tempPoint -= 1;
                tempATK += 1;
            }
        }
        else
        {
            if (tempATK > MainData.Instance.MinATK)
            {
                tempPoint += 1;
                tempATK -= 1;
            }
        }
        Refresh();
    }

    public void OnGoNext()
    {
        go_EnhancePage.SetActive(true);
        
    }

    IEnumerator WaitTime(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }
}
