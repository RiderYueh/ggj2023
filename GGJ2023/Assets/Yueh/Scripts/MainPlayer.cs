using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainPlayer : MonoBehaviour
{
    public static MainPlayer instance;
    public bool isWild = false;

    private int villiageNum = 0;
    public Platformer.Mechanics.PlayerController controller;

    public GameObject goFade;
    public Cinemachine.CinemachineConfiner mainCC;
    public PolygonCollider2D polyVilldage00;
    public PolygonCollider2D polyVilldage01;


    
    private Animator mainAnimator;
    public GameObject go_AttackArea;

    private SpriteRenderer m_SpriteRenderer;

    public List<GameObject> go_list_item;

    //========ui
    public Image img_HP;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        foreach(int id in MainData.Instance.list_GetItem)
        {
            go_list_item[id].SetActive(true);
        }
        mainAnimator = GetComponent<Animator>();
        MainData.Instance.HP = MainData.Instance.MaxHP;
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        UpdateHP();

    }

    private void Update()
    {
        invincibleTime -= Time.deltaTime;
        if (isWild)
        {
            
        }
        else
        {
            if (!isFade)
            {
                CheckPos();
            }
        }
        
        CheckGoldHand();
    }

    private void CheckGoldHand()
    {
        /*if(Input.GetKeyDown("f"))
        {
            OnFade();
        }
        if(Input.GetKeyDown("m"))
        {
            MainData.Instance.isMissing = true;
            Debug.Log("isMissing");
        }*/
        
        if(Input.GetMouseButtonDown(0))
        {
            if (isWild)
            {
                OnAttack();
            }
            
        }
        
        if(Input.GetKeyDown("p"))
        {
            MainData.Instance.Point += 1;
        }

        if(Input.GetKeyDown("l"))
        {
            SceneManager.LoadScene(6);
        }
        if (Input.GetKeyDown("k"))
        {
            MainData.Instance.StateNow = 0;
            SceneManager.LoadScene(6);
        }
    }

    bool isAttacking = false;
    private void OnAttack()
    {
        mainAnimator.SetBool("isAttack" , true);
    }
    private void OnAttackEnd()
    {
        isAttacking = false;
        mainAnimator.SetBool("isAttack", false);
        go_AttackArea.SetActive(false);
    }

    private void OnAttacking()
    {
        isAttacking = true;
        go_AttackArea.SetActive(true);
    }

    public GameObject go_dialog;

    private void CheckPos()
    {
        if(villiageNum == 0)
        {
            if (transform.position.x > 5)
            {
                ChangeValliage(1);
                OnFade();
            }
            if (transform.position.x < -32)
            {
                controller.controlEnabled = false;
                go_dialog.SetActive(true);
               

            }

        }
        else if (villiageNum == 1)
        {
            if (transform.position.x < 6)
            {
                ChangeValliage(0);
                OnFade();
            }
        }
    }

    public void Diactive_btn()//關閉對話框後修改位置
    {
        transform.position = new Vector3(-25,transform.position.y,transform.position.z);
        controller.controlEnabled = true;

    }

    public void Active_btn()
    {
        if(MainData.Instance.list_GetItem.Count >= 4)
        {
            SceneManager.LoadScene(8);
        }
        else
        {
            SceneManager.LoadScene(MainData.Instance.StateNow + 1);
        }
    }

    private void ChangeValliage(int num)
    {
        if(num == 0)
        {
            villiageNum = 0;
            StartCoroutine(WaitTime(0.3f, () => {
                mainCC.m_BoundingShape2D = polyVilldage00;
                transform.position = new Vector3(3, transform.position.y, transform.position.z);
            }));
        }
        if (num == 1)
        {
            if (MainData.Instance.isMissing)
            {
                StartCoroutine(WaitTime(0.3f, () => {
                    transform.position = new Vector3(-25, transform.position.y, transform.position.z);
                }));
            }
            else
            {
                villiageNum = 1;
                StartCoroutine(WaitTime(0.3f, () => {
                    mainCC.m_BoundingShape2D = polyVilldage01;
                    transform.position = new Vector3(10, transform.position.y, transform.position.z);
                }));
            }
            
            
        }
    }

    bool isFade = false;
    private void OnFade()
    {
        isFade = true;
        goFade.SetActive(true);
        controller.controlEnabled = false;
        StartCoroutine(WaitTime(0.6f, ()=> {
            goFade.SetActive(false);
            isFade = false;
            controller.controlEnabled = true;
        }));
    }

    IEnumerator WaitTime(float timer , UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    bool isShopping = false;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Item")
        {
            if(Input.GetMouseButton(0))
            {
                int id = collision.GetComponent<Item>().ItemID;
                MainData.Instance.list_GetItem.Add(id);
                go_list_item[id].SetActive(true);
                Debug.Log("撿到道具 : " + id);
                AudioManager.instance.PlaySound(AudioManager.ESound.Pick);
                Destroy(collision.gameObject);
            }
        }
        if (collision.tag == "NPC")
        {
            if (Input.GetMouseButton(0))
            {
                if (isShopping) return;
                isShopping = true;
                OpenShop();
            }
        }
    }
    public NPCDialog m_NPCDialog;
    public void OpenShop()
    {
        controller.controlEnabled = false;
        m_NPCDialog.gameObject.SetActive(true);
    }

    public void CloseShop()
    {
        controller.controlEnabled = true;
        m_NPCDialog.gameObject.SetActive(false);
        isShopping = false;
        UpdateHP();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if(isAttacking)
            {
                Debug.Log("Attacked");
                AudioManager.instance.PlaySound(AudioManager.ESound.Attack , 0.15f);
                SlowMotion();
                collision.GetComponent<EnemyBe>().OnAttacked();
            }
        }
    }

    bool isSlowing = false;
    public void SlowMotion(float ts = 0.1f, float timer = 0.02f)
    {
        if (isSlowing) return;
        isSlowing = true;
        Time.timeScale = 0.1f;

        StartCoroutine(WaitTime(timer, () =>
        {
            Time.timeScale = 1f;
            isSlowing = false;
        }));
    }

    float invincibleTime = 0;

    public void OnHurted(int hurt = 1)
    {
        if(invincibleTime > 0)
        {
            return;
        }
        invincibleTime = 1f;
        OnHurtSprite();
        SlowMotion();
        Debug.Log("Hurt");
        AudioManager.instance.PlaySound(AudioManager.ESound.Hurt);
        MainData.Instance.HP -= hurt;
        UpdateHP();
    }

    public void UpdateHP()
    {
        img_HP.fillAmount = (float)MainData.Instance.HP / (float)MainData.Instance.UI_HP;
    }

    public void OnHurtSprite()
    {
        m_SpriteRenderer.color = new Color(255, 0, 0, 0.5f);

        StartCoroutine(WaitTime(0.2f, () => {
            m_SpriteRenderer.color = new Color(255, 255, 255, 0.5f);
        }));

        StartCoroutine(WaitTime(1, () => {
            m_SpriteRenderer.color = Color.white;
        }));
    }
}
