﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class CountScene : MonoBehaviour
{
    

    public Text mainText;
    public Text textHP;
    public Text textATK;
    public VolumeProfile vp2;
    public Volume cameraVolume;
    public SpriteRenderer m_SpriteRenderer;
    public Sprite BG;
    public GameObject fireParticle;

    public List<GameObject> list_item;

    void Start()
    {
        MainData.Instance.StateNow += 1;
        MainData.Instance.Point += 3;
        Debug.Log("state : " + MainData.Instance.StateNow);
        ChangeSceneArt();
    }

    void ChangeSceneArt()
    {
        if(MainData.Instance.StateNow == 1)
        {
            list_item[0].SetActive(true);
            AudioManager.instance.PlaySound(AudioManager.ESound.BGM01);
        }
        else if (MainData.Instance.StateNow == 2)
        {
            list_item[1].SetActive(true);
            AudioManager.instance.PlaySound(AudioManager.ESound.BGM02);
        }
        else if (MainData.Instance.StateNow == 3)
        {
            list_item[2].SetActive(true);
            AudioManager.instance.PlaySound(AudioManager.ESound.BGM03);
        }
        else if (MainData.Instance.StateNow >= 4)
        {
            list_item[3].SetActive(true);
            AudioManager.instance.PlaySound(AudioManager.ESound.BGM04);
        }

        if (MainData.Instance.StateNow >1 )
        {
            mainText.text = "闀囧崄瀛楄矾鍙ｅ\n悜鍖?00绫?楂樺";
            textHP.text = "è³";
            textATK.text = "èï";
        }
        if (MainData.Instance.StateNow > 2)
        {
            cameraVolume.profile = vp2;
        }
        if (MainData.Instance.StateNow > 3)
        {
            m_SpriteRenderer.sprite = BG;
            fireParticle.SetActive(true);
            MainData.Instance.isMissing = true;
        }
    }
}
