using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ShowText2 : MonoBehaviour
{
    public List<GameObject> list_NeedShow;

    public float speed = 2f;
    
    void Start()
    {
        float time = 2;

        for (int i = 0; i < list_NeedShow.Count; i++)
        {
            GameObject go = list_NeedShow[i];
            StartCoroutine(WaitTime(time, () => {
                go.gameObject.SetActive(true);
            }));
            time += speed;

        }
    }


    IEnumerator WaitTime(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    public void OnExit()
    {
        Application.Quit();
    }

    public void OnReplay()
    {
        MainData.Instance.Reset();
        SceneManager.LoadScene(0);

    }
}
