using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainData
{

    //State============
    public bool isMissing = false;
    public int MaxHP = 5;
    public int UI_HP = 14;
    public int HP = 5;
    public int ATK = 1;
    public int Point = 0;

    public int MinATK = 1;
    public int MinHP = 5;

    public bool winBattle = false;

    public int StateNow = 0;
    public List<int> list_GetItem;

    public bool[] learntSkill;
    public float[] skillDmg;
    public float[] skillInterval;
    public float[] skillSize;
    public float[] skillDuration;

    private static MainData _instance;
    public static MainData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MainData();
            }
            return _instance;
        }
    }

    public void Init()
    {
        list_GetItem = new List<int>();
    }

    public void Reset() //會把整個遊戲從第一關數值開始
    {
        isMissing = false;
        MaxHP = 5;
        HP = 5;
        ATK = 1;
        Point = 0;
        MinATK = 1;
        MinHP = 5;
        StateNow = 0;
        list_GetItem = new List<int>();
    }

    public void StageRePlay()
    {
        HP = MaxHP;
    }

}


public class RunFirst
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void OnBeforeSceneLoadRuntimeMethod()
    {
        MainData.Instance.Init();
    }
}