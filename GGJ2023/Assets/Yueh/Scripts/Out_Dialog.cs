using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Out_Dialog : MonoBehaviour
{
    public GameObject go_dialog;

    public void OnBtnYes()
    {
        AudioManager.instance.PlaySound(AudioManager.ESound.Click);
        MainPlayer.instance.Active_btn();
    }

    public void OnBtnNo()
    {
        AudioManager.instance.PlaySound(AudioManager.ESound.Click);
        go_dialog.SetActive(false);
        MainPlayer.instance.Diactive_btn();
    }
}
